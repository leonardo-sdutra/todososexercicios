package ex10;

public abstract class Bolo
{
    abstract void cobertura();
    abstract void ingredientes();
    abstract void tempo();
    
    protected void misturar()
    {
        System.out.println("Inserir ingredientes na batedeira para misturar.");
    };
    
    protected void forno()
    {
        System.out.println("Inserir no forno para assar.");
    }
    
    protected void desenformar()
    {
            System.out.println("Remover bolo da forma.");
    }
    
    protected void exec()
    {
        this.misturar();
        this.ingredientes();
        this.forno();
        this.tempo();
        this.desenformar();
        this.cobertura();
    };    
}
