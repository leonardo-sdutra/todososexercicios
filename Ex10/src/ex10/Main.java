package ex10;

public class Main
{
    public static void main(String[] args)
    {
        Bolo boloDeChocolate = new BoloDeChocolate();
        Bolo boloDeMilho = new BoloDeMilho();

        System.out.print("Bolo de chocolate:\n\n");
        boloDeChocolate.exec();
        
        System.out.print("Bolo de milho:\n\n");
        boloDeMilho.exec();
    }
}
