package ex10;

public class BoloDeMilho extends Bolo
{
    @Override
    void ingredientes()
    {
        System.out.println("Três ovos;\n" +
                           "300g de milho;\n" +
                           "Uma xícara de açucar;\n" +
                           "Duas xícaras de azeite;\n" +
                           "Uma xícara de leite.\n");
    }
    
    @Override
    void tempo()
    {
        System.out.println("Assar por quarenta minutos.\n");
    }
    @Override
    void cobertura()
    {
        System.out.println("Não há cobertura.\n");
    }
}
