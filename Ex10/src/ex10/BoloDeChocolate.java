package ex10;

public class BoloDeChocolate extends Bolo
{
    @Override
    void ingredientes()
    {
        System.out.println("Quatro ovos;\n" +
                           "200g de chocolate;\n" +
                           "Uma xícara de açucar;\n" +
                           "Duas xícaras de farinha;\n" +
                           "Uma xícara de água.\n");
    }
    
    @Override
    void tempo()
    {
        System.out.println("Assar por trinta minutos.\n");
    }
    
    @Override
    void cobertura()
    {
        System.out.println("Uma caixa de leite condensado;\n" +
                           "Cinco colheres de sopa de chocolate em pó;\n" +
                           "Uma xícara de leite;\n");
    }
}
