package ex12;

import java.util.ArrayList;
import java.util.List;

public abstract class Visitor
{
    public List<Record> orderedLog;
    public abstract void visit(VirtualDataLogger v);
    
    public Visitor()
    {
        orderedLog = new ArrayList<Record>();
    }
}
