package ex12;

public class Record
{
    private float temperature;
    private int time;
    
    public Record(float temperature, int time)
    {
        this.temperature = temperature;
        this.time = time;
    }

    Record()
    {
        
    }

    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    /**
     * @param time the time to set
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * @return the temperature
     */
    public float getTemperature() {
        return temperature;
    }

    /**
     * @return the time
     */
    public int getTime() {
        return time;
    }
}
