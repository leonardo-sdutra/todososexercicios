package ex12;

import java.util.List;

public abstract class VirtualDataLogger
{
    public List<Record> log;
    public abstract void accept(Visitor v);
    public abstract List<Record> getLog();
    public abstract void setLog(float temperature, int time);
    
}
