package ex12;

import java.util.Collections;

public class OrderVisitor extends Visitor
{
    @Override
    public void visit(VirtualDataLogger v)
    {
        for(int i = 0; i < orderedLog.size(); i++)
        {
            for(int j = 0; j < orderedLog.size(); j++)
            {
                if(orderedLog.get(i).getTemperature() < orderedLog.get(j).getTemperature())
                {
                    float aux = orderedLog.get(i).getTemperature();
                    orderedLog.get(i).setTemperature(orderedLog.get(j).getTemperature());
                    orderedLog.get(j).setTemperature(aux);
                }
            }
        }
    }
}
