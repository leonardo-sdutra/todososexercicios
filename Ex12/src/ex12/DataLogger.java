package ex12;

import java.util.ArrayList;
import java.util.List;

public class DataLogger extends VirtualDataLogger
{
    public DataLogger()
    {
        this.log = new ArrayList<Record>();
    }
    
    @Override
    public void accept(Visitor v)
    {
        v.visit(this);
    }

    @Override
    public List<Record> getLog()
    {
        return this.log;
    }
    
    @Override
    public void setLog(float temperature, int time)
    {
        Record r = new Record();
        r.setTemperature(temperature);
        r.setTime(time);

        log.add(r);
    }

}
