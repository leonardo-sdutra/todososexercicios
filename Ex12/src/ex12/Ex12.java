package ex12;

import java.util.Iterator;
import java.util.List;

public class Ex12
{
    public static void main(String[] args)
    {
        DataLogger dataLogger = new DataLogger();
        OrderVisitor order = new OrderVisitor();
        List<Record> temp;
        
        dataLogger.setLog(37.8f, 1);
        dataLogger.setLog(34.9f, 2);
        dataLogger.setLog(41.8f, 3);
        dataLogger.setLog(30.7f, 4);
        dataLogger.setLog(25.5f, 5);
        dataLogger.setLog(31.9f, 6);
        
        temp = dataLogger.getLog();
        
        System.out.println("Log:");
        
        for(Record record : temp)
            System.out.println(record.getTime() + "h, " + record.getTemperature() + "ºC");
    }
}
