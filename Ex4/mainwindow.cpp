#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    QString text = ui->textEdit->toPlainText();
    QString subString = ui->lineEdit->text();
    int count;

    bool isCaseSensitive = ui->checkBox->isChecked();

    if(isCaseSensitive)
        count = text.count(subString, Qt::CaseSensitive);

    else
        count = text.count(subString, Qt::CaseSensitive);

    if(count == 0)
    {
        ui->lineEdit_2->setText("No occurences were found.");
        return;
    }

    ui->lineEdit_2->setText("Found " + QString::number(count) + " occurences");

    ui->textEdit->clear();

    if(isCaseSensitive)
        ui->textEdit->setText(text.replace(subString, "<font color=\"#FF0000\">" + subString + "</font>", Qt::CaseSensitive));

    else
        ui->textEdit->setText(text.replace(subString, "<font color=\"#FF0000\">" + subString + "</font>", Qt::CaseInsensitive));
}
