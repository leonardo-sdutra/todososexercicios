#ifndef MINIMUM_H
#define MINIMUM_H


class Minimum
{
public:
    Minimum() {}

    float calc(float *values, int length)
    {
        float minimum = values[0];

        for(int i = 0; i < length; i++)
        {
            if(values[i] < minimum)
                minimum = values[i];
        }
        return minimum;
    }
};

#endif // MINIMUM_H
