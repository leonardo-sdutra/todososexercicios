#ifndef MEAN_H
#define MEAN_H


class Mean
{
public:
    Mean() {}

    calc(float *values, int length)
    {
        float mean = 0.0f;

        for(int i = 0; i < length; i++)
            mean += values[i];

        return mean/(float)length;
    }
};

#endif // MEAN_H
