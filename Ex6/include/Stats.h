#ifndef STATS_H
#define STATS_H

#include "Minimum.h"
#include "Maximum.h"
#include "Mean.h"

class Stats
{
public:
    Stats() {}

    float* calc(float *values, int length)
    {
        static float result[3];

        Mean mean_;
        result[0] = mean_.calc(values, length);

        Minimum minimum_;
        result[1] = minimum_.calc(values, length);

        Maximum maximum_;
        result[2] = maximum_.calc(values, length);

        return result;
    }
};

#endif // STATS_H
