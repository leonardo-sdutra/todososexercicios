#ifndef MAXIMUM_H
#define MAXIMUM_H

class Maximum
{
public:
    Maximum() {}

    float calc(float *values, int length)
    {
        float maximum = values[0];

        for(int i = 0; i < length; i++)
        {
            if(values[i] > maximum)
                maximum = values[i];
        }
        return maximum;
    }
};

#endif // MAXIMUM_H
