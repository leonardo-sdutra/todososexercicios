#include <iostream>
#include "Stats.h"

using namespace std;

int main()
{
    float values[10] = {4, 9, 3, 3, 6, 3, 7, 8, 6, 5};

    Stats stats;

    float* result = stats.calc(values, 10);

    cout << "Values: ";
    for(int i = 0; i < 10; i++)
        cout << values[i] << " ";
    cout << "\n\n";

    cout << "Statistics:" << endl
         << "Mean: "      << result[0] << endl
         << "Minimum: "   << result[1] << endl
         << "Maximum: "   << result[2] << endl;

    return 0;
}
