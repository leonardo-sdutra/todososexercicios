#include <iostream>
#include "Singleton.h"

using namespace std;

int main()
{
    Singleton *a = Singleton::getInstance();
    Singleton *b = Singleton::getInstance();

    cout << "Endereco de a: " << a << endl;
    cout << "Endereco de b: " << b << endl;

    return 0;
}
