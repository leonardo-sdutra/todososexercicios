#include <iostream>
#include <Time.h>
#include <CivilianTime.h>
#include <ZuluTime.h>
#include <BrazilianTime.h>

using namespace std;

int main()
{
    Time *times[4];
    times[0] = new Time(12, 20);
    times[1] = new CivilianTime(2, 30, true);
    times[2] = new ZuluTime(3, 30, 5);
    times[3] = new BrazilianTime(20, 7);

    for(int i = 0; i < 4; i++)
        times[i]->tell();

    return 0;
}
