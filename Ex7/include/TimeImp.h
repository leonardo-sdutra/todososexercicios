#ifndef TIMEIMP_H
#define TIMEIMP_H

#include <iostream>
#include <iomanip>
#include <string.h>

using namespace std;

class TimeImp
{
public:
    TimeImp(int hr, int min)
    {
        hr_ = hr;
        min_ = min;
    }

    virtual void tell()
    {
        cout << "Time is " << hr_ << ":" << min_ << endl;
    }

protected:
    int hr_;
    int min_;
};

#endif // TIMEIMP_H
