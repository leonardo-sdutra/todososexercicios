#ifndef BRAZILIANTIME_H
#define BRAZILIANTIME_H

#include <Time.h>
#include <BrazilianTimeImp.h>

class BrazilianTime : public Time
{
public:
    BrazilianTime(int hr, int min)
    {
        imp_ = new BrazilianTimeImp(hr, min);
    }
};

#endif // BRAZILIANTIME_H
