#ifndef CIVILIANTIME_H
#define CIVILIANTIME_H

#include <Time.h>
#include <CivilianTimeImp.h>

class CivilianTime : public Time
{
public:
    CivilianTime(int hr, int min, bool pm)
    {
        imp_ = new CivilianTimeImp(hr, min, pm);
    }

};

#endif // CIVILIANTIME_H
