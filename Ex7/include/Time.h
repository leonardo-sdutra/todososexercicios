#ifndef TIME_H
#define TIME_H

#include <TimeImp.h>

class Time
{
public:
    Time() {}
    Time(int hr, int min)
    {
        imp_ = new TimeImp(hr, min);
    }

    virtual void tell()
    {
        imp_->tell();
    }

protected:
    TimeImp *imp_;
};

#endif // TIME_H
