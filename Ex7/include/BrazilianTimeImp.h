#ifndef BRAZILIANTIMEIMP_H
#define BRAZILIANTIMEIMP_H

#include <TimeImp.h>


class BrazilianTimeImp : public TimeImp
{
public:
    BrazilianTimeImp(int hr, int min) : TimeImp(hr, min)
    {

    }

    void tell()
    {
        cout << "Time is " << hr_ << ":" << min_ << " BRT" << endl;
    }
};

#endif // BRAZILIANTIMEIMP_H
