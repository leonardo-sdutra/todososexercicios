#ifndef CIVILIANTIMEIMP_H
#define CIVILIANTIMEIMP_H

#include <TimeImp.h>

using namespace std;

class CivilianTimeImp : public TimeImp
{
public:
    CivilianTimeImp(int hr, int min, bool pm) : TimeImp(hr, min)
    {
        if(pm)
            strcpy(whichM_, " PM");
        else
            strcpy(whichM_, " AM");
    }

    void tell()
    {
        cout << "Time is " << hr_ << ":" << min_ << whichM_ << endl;
    }

protected:
    char whichM_[4];
};

#endif // CIVILIANTIMEIMP_H
