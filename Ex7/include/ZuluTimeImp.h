#ifndef ZULUTIMEIMP_H
#define ZULUTIMEIMP_H

#include <iostream>
#include <TimeImp.h>

using namespace std;

class ZuluTimeImp : public TimeImp
{
public:
    ZuluTimeImp(int hr, int min, int zone) : TimeImp(hr, min)
    {
        if(zone == 5)
            strcpy(zone_, " Eastern Standard Time");
        else if(zone == 6)
            strcpy(zone_, " Central Standard Time");
    }

    void tell()
    {
        cout << "Time is " << hr_ << ":" << min_ << zone_ << endl;
    }

protected:
    char zone_[30];
};

#endif // ZULUTIMEIMP_H
