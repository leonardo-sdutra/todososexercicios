#ifndef ZULUTIME_H
#define ZULUTIME_H

#include <Time.h>
#include <ZuluTimeImp.h>

class ZuluTime : public Time
{
public:
    ZuluTime(int hr, int min, int zone)
    {
        imp_ = new ZuluTimeImp(hr, min, zone);
    }
};

#endif // ZULUTIME_H
