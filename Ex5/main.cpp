#include <iostream>
#include <Rectangle.h>
#include <RectangleAdapter.h>
using namespace std;

int main()
{
    int x = 1, y = 2, w = 10, h = 30;
    Rectangle *rect = new RectangleAdapter(x,y,w,h);
    rect->draw();
}
