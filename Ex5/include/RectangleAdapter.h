#ifndef RECTANGLEADAPTER_H
#define RECTANGLEADAPTER_H

#include <Rectangle.h>
#include <LegacyRectangle.h>
#include <iostream>

using namespace std;

class RectangleAdapter : public Rectangle, private LegacyRectangle
{
public:
    RectangleAdapter(int x, int y, int w, int h) :
        LegacyRectangle(x, y, x+w, y+h) {cout << "RectangleAdapter(x, y, x+w, y+h)" << endl;}

    void draw()
    {
        cout << "RectangleAdapter: draw()" << endl;
        oldDraw();
    }
};

#endif // RECTANGLEADAPTER_H
