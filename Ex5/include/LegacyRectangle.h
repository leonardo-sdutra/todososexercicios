#ifndef LEGACYRECTANGLE_H
#define LEGACYRECTANGLE_H

#include <iostream>

using namespace std;

// Adaptee
class LegacyRectangle
{
public:
    LegacyRectangle(int x1, int y1, int x2, int y2)
    {
        x1_ = x1;
        y1_ = y1;
        x2_ = x2;
        y2_ = y2;

        cout << "LegacyRectangle(x1, y1, x2, y2)" << endl;
    }

    void oldDraw()
    {
        cout << "LegacyRectangle: oldDraw()" << endl;
    }

private:
    int x1_;
    int y1_;
    int x2_;
    int y2_;
};

#endif // LEGACYRECTANGLE_H
