#ifndef RECTANGLE_H
#define RECTANGLE_H

// Target
class Rectangle
{
public:
    virtual void draw() = 0;
};

#endif // RECTANGLE_H
