#include <iostream>
#include "GUIFactory.h"
#include "Buttons.h"

using namespace std;

int main()
{
    Factory f;
    Button *button;

    button = f.createButton("windows");
    button->paint();
    button = f.createButton("osx");
    button->paint();

    return 0;
}
