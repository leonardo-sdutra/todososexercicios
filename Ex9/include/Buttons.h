#ifndef BUTTONS_H_INCLUDED
#define BUTTONS_H_INCLUDED

#include <iostream>

using namespace std;

class Button
{
public:
    virtual void paint() = 0;
};


class WindowsButton : public Button
{
public:
    void paint()
    {
        cout << "Drawing windows button" << endl;
    };
};


class OSXButton : public Button
{
public:
    void paint()
    {
        cout << "Drawing OSX button" << endl;
    };
};

#endif // BUTTONS_H_INCLUDED
