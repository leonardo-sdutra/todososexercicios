#ifndef GUIFACTORY_H
#define GUIFACTORY_H

#include <string.h>
#include "Buttons.h"

class GUIFactory
{
public:
    virtual Button* createButton(const char*) = 0;
};


class Factory : public GUIFactory
{
public:
    Button* createButton(const char* type)
    {
        if(!strcmp(type, "windows"))
            return new WindowsButton;

        else if(!strcmp(type, "osx"))
            return new OSXButton;

        else
            return 0;
    }
};


#endif // GUIFACTORY_H
