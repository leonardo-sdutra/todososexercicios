#include "molho_tomate.h"

molho_tomate::molho_tomate(pizza *pizza_dec): pizza_decorator(pizza_dec)
{
    setKcal(5);
    setPreco(3.00);
}

int molho_tomate::getKcal()
{
    return kcal + pizza_dec->getKcal();
}

float molho_tomate::getPreco()
{
    return preco + pizza_dec->getPreco();
}
