#include "cebola.h"

cebola::cebola(pizza *pizza_dec): pizza_decorator(pizza_dec)
{
    setKcal(5);
    setPreco(1.00);
}

int cebola::getKcal()
{
    return kcal + pizza_dec->getKcal();
}

float cebola::getPreco()
{
    return preco + pizza_dec->getPreco();
}
