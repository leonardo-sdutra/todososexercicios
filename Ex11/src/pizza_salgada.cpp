#include "pizza_salgada.h"

pizza_salgada::pizza_salgada()
{
    setKcal(120);
    setPreco(20.00);
}

int pizza_salgada::getKcal()
{
    return kcal;
}

float pizza_salgada::getPreco()
{
    return preco;
}
