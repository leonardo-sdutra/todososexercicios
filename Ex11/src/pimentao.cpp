#include "pimentao.h"

pimentao::pimentao(pizza *pizza_dec): pizza_decorator(pizza_dec)
{
    setKcal(10);
    setPreco(2.00);
}

int pimentao::getKcal()
{
    return kcal + pizza_dec->getKcal();
}

float pimentao::getPreco()
{
    return preco + pizza_dec->getPreco();
}
