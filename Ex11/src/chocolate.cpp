#include "chocolate.h"

chocolate::chocolate(pizza *pizza_dec): pizza_decorator(pizza_dec)
{
    setKcal(150);
    setPreco(9.00);
}

int chocolate::getKcal()
{
    return kcal + pizza_dec->getKcal();
}

float chocolate::getPreco()
{
    return preco + pizza_dec->getPreco();
}
