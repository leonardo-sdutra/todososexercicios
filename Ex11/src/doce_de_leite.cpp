#include "doce_de_leite.h"

doce_de_leite::doce_de_leite(pizza *pizza_dec): pizza_decorator(pizza_dec)
{
    setKcal(190);
    setPreco(8.00);
}

int doce_de_leite::getKcal()
{
    return kcal + pizza_dec->getKcal();
}

float doce_de_leite::getPreco()
{
    return preco + pizza_dec->getPreco();
}
