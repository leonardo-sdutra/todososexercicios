#include "calabresa.h"

calabresa::calabresa(pizza *pizza_dec):pizza_decorator(pizza_dec)
{
    setKcal(80);
    setPreco(6.00);
}

int calabresa::getKcal()
{
    return (kcal + pizza_dec->getKcal());
}

float calabresa::getPreco()
{
    return (preco + pizza_dec->getPreco());
}
