#include "morangos.h"

morangos::morangos(pizza *pizza_dec): pizza_decorator(pizza_dec)
{
    setKcal(30);
    setPreco(5.00);
}

int morangos::getKcal()
{
    return kcal + pizza_dec->getKcal();
}

float morangos::getPreco()
{
    return preco + pizza_dec->getPreco();
}
