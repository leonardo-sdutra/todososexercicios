#include "confeitos.h"

confeitos::confeitos(pizza *pizza_dec): pizza_decorator(pizza_dec)
{
    setKcal(20);
    setPreco(2.00);
}

int confeitos::getKcal()
{
    return kcal + pizza_dec->getKcal();
}

float confeitos::getPreco()
{
    return preco + pizza_dec->getPreco();
}
