#include "porcao_queijo.h"

porcao_queijo::porcao_queijo(pizza *pizza_dec): pizza_decorator(pizza_dec)
{
    setKcal(100);
    setPreco(5.00);
}

int porcao_queijo::getKcal()
{
    return (kcal + pizza_dec->getKcal());
}

float porcao_queijo::getPreco()
{
    return preco + pizza_dec->getPreco();
}
