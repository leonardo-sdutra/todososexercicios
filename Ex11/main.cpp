#include <iostream>
#include  <stdlib.h>
#include "pizza.h"
#include "pizza_doce.h"
#include "pizza_salgada.h"
#include "pizza_decorator.h"
#include "calabresa.h"
#include "porcao_queijo.h"
#include "molho_tomate.h"
#include "cebola.h"
#include "pimentao.h"
#include "doce_de_leite.h"
#include "morangos.h"
#include "chocolate.h"
#include "confeitos.h"

using namespace std;

/********POR LEONARDO DUTRA E GUSTAVO GOETZ********/

void menu_1()
{
    cout << "Inicio: " << endl
    << endl
    << "Escolha o tipo da pizza: " << endl
    << endl
    << "1 - Pizza Doce" << endl
    << "2 - Pizza Salgada" << endl;

}

void menu_2()
{
    cout << "Escolha os ingredientes: " << endl
    << endl
    << "1 - Porcao de Queijo" << endl
    << "2 - Molho de Tomate" << endl
    << "3 - Calabresa" << endl
    << "4 - Cebola" << endl
    << "5 - Pimentao" << endl
    << "6 - Doce de Leite" << endl
    << "7 - Morangos" << endl
    << "8 - Chocolate" << endl
    << "9 - Confeitos" << endl
    << "0 - Finalizar pedido" << endl;
}
void limpaTela()
{
    system("cls");
}
int main()
{
    int op = NULL;
    pizza *test;
    menu_1();


    while(op != 99){
    cin >> op;
    switch (op){
        case 1:
        test = new pizza_doce;
        op=99;
        case 2:
        test = new pizza_salgada;
        op=99;
        default:
            limpaTela();
            cout << "Opcao invalida!" << endl
            << "Secione uma das opcoes (1/2)" << endl;
            menu_1();
    }
    }

    limpaTela();
    while(op !=0)
    {
        limpaTela();
        menu_2();
        cout << endl << "Valor: R$ " << test->getPreco() << endl
        << "Kcal: " << test->getKcal() << endl;

        cin >> op;

        switch (op){
        case 0:
            limpaTela();
            break;
        case 1:
            test = new porcao_queijo(test);
            break;
        case 2:
            test = new molho_tomate(test);
            break;
        case 3:
            test = new calabresa(test);
            break;
        case 4:
            test = new cebola(test);
            break;
        case 5:
            test = new pimentao(test);
            break;
        case 6:
            test = new doce_de_leite(test);
            break;
        case 7:
            test = new morangos(test);
            break;
        case 8:
            test = new chocolate(test);
            break;
        case 9:
            test = new confeitos(test);
            break;
        default:
            cout << "Opcao invalida!!" << endl;
        }
    }
    limpaTela();
    cout << endl << "Valor: R$ " << test->getPreco() << endl
        << "Kcal: " << test->getKcal() << endl;

    return 0;
}
