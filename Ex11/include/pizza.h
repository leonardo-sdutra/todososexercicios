#ifndef PIZZA_H
#define PIZZA_H


class pizza
{
    public:
        pizza();
        virtual float getPreco() = 0;
        virtual int getKcal() = 0;
        void setPreco(float preco);
        void setKcal(int kcal);

    protected:
        int kcal;
        float preco;
};

#endif // PIZZA_H
