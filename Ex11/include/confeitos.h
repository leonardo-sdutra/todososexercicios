#ifndef CONFEITOS_H
#define CONFEITOS_H
#include "pizza.h"
#include "pizza_decorator.h"

class confeitos:public pizza_decorator
{
    public:
        confeitos(pizza *pizza_dec);
        int getKcal();
        float getPreco();


    protected:

    private:
};

#endif // CONFEITOS_H
