#ifndef PIZZA_SALGADA_H
#define PIZZA_SALGADA_H
#include "pizza.h"

class pizza_salgada: public pizza
{
    public:
        pizza_salgada();
        float getPreco();
        int getKcal();

    protected:

    private:
};

#endif // PIZZA_SALGADA_H
