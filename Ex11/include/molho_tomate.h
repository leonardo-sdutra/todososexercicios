#ifndef MOLHO_TOMATE_H
#define MOLHO_TOMATE_H
#include "pizza.h"
#include "pizza_decorator.h"

class molho_tomate:public pizza_decorator
{
    public:
        molho_tomate(pizza *pizza_dec);
        int getKcal();
        float getPreco();

    protected:

    private:
};

#endif // MOLHO_TOMATE_H
