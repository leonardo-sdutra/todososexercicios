#ifndef CALABRESA_H
#define CALABRESA_H
#include "pizza.h"
#include "pizza_decorator.h"

class calabresa : public pizza_decorator
{
    public:
        calabresa(pizza *pizza_dec);
        int getKcal();
        float getPreco();

    protected:

    private:
};

#endif // CALABRESA_H
