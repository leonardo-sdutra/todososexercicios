#ifndef PIZZA_DECORATOR_H
#define PIZZA_DECORATOR_H
#include "pizza.h"

class pizza_decorator : public pizza
{
    public:
        pizza_decorator(pizza *pizza_dec);

    protected:
        pizza *pizza_dec;

};

#endif // PIZZA_DECORATOR_H
