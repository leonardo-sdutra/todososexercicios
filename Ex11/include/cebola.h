#ifndef CEBOLA_H
#define CEBOLA_H
#include "pizza.h"
#include "pizza_decorator.h"

class cebola:public pizza_decorator
{
    public:
        cebola(pizza *pizza_dec);
        int getKcal();
        float getPreco();


    protected:

    private:
};

#endif // CEBOLA_H
