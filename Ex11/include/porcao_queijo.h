#ifndef PORCAO_QUEIJO_H
#define PORCAO_QUEIJO_H
#include "pizza.h"
#include "pizza_decorator.h"

class porcao_queijo: public pizza_decorator
{
    public:
        porcao_queijo(pizza *pizza_dec);
        int getKcal();
        float getPreco();

    protected:

    private:
};

#endif // PORCAO_QUEIJO_H
