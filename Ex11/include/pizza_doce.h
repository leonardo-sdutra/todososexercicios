#ifndef PIZZA_DOCE_H
#define PIZZA_DOCE_H

#include "pizza.h"

class pizza_doce: public pizza
{
    public:
        pizza_doce();
        int getKcal();
        float getPreco();

    protected:

    private:
};

#endif // PIZZA_DOCE_H
