#ifndef CHOCOLATE_H
#define CHOCOLATE_H
#include "pizza.h"
#include "pizza_decorator.h"

class chocolate:public pizza_decorator
{
    public:
        chocolate(pizza *pizza_dec);
        int getKcal();
        float getPreco();


    protected:

    private:
};

#endif // CHOCOLATE_H
