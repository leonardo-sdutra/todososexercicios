#ifndef DOCE_DE_LEITE_H
#define DOCE_DE_LEITE_H
#include "pizza.h"
#include "pizza_decorator.h"

class doce_de_leite:public pizza_decorator
{
    public:
        doce_de_leite(pizza *pizza_dec);
        int getKcal();
        float getPreco();

    protected:

    private:
};

#endif // DOCE_DE_LEITE_H
