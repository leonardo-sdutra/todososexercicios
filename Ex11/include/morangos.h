#ifndef MORANGOS_H
#define MORANGOS_H
#include "pizza.h"
#include "pizza_decorator.h"

class morangos:public pizza_decorator
{
    public:
        morangos(pizza *pizza_dec);
        int getKcal();
        float getPreco();

    protected:

    private:
};

#endif // MORANGOS_H
