#ifndef PIMENTAO_H
#define PIMENTAO_H
#include "pizza.h"
#include "pizza_decorator.h"

class pimentao:public pizza_decorator
{
    public:
        pimentao(pizza *pizza_dec);
        int getKcal();
        float getPreco();

    protected:

    private:
};

#endif // PIMENTAO_H
